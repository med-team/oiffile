#!/usr/bin/env python

import oiffile
import numpy

if __name__ == "__main__":
    import doctest
    numpy.set_printoptions(suppress=True, precision=5)
    doctest.testmod()
